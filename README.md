# Overview #

TikZposter is a [LaTeX](http://en.wikipedia.org/wiki/LaTeX) class to create scientific posters in an efficient way.
It uses [TikZ](http://en.wikipedia.org/wiki/PGF/TikZ) to provide graphics for the different poster elements.
A main goal of TikZposter is the allocation of dynamic and easy to change posters.

### How do I get set up? ###

* TikZposter is accessible on [CTAN](http://www.ctan.org/pkg/tikzposter)
* Useful information are available in the [Wiki](https://bitbucket.org/surmann/tikzposter/wiki)

### Contribution guidelines ###

* Bugs are stored in the [Issue](https://bitbucket.org/surmann/tikzposter/issues) database
* You are welcome to provide a pull that solves a problem or improves TikZposter